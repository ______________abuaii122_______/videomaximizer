

//~ com.b2a.repeatFor(
      //~ function() { return com.b2a.hasShit([]); }, 
      //~ function() { chrome.extension.sendRequest({}, function(response) {}); },
      //~ 1000,
      //~ 10,
      //~ 0);

com.b2a["contentPage"] = {
  dataObjects : [],
  exclusions : [
    { host: "www.hulu.com", path : "/stand_alone"},
    { host: "www.hulu.com", path : "/profile/queue"}
  ],
  cleanExclusions : function()
  {
    return this.exclusions.map(function(x){
      var newExclusion = { host : null, path : null };
      if (x.host != null)
      {
        newExclusion.host = x.host.toLowerCase();
      }
      if (x.path != null)
      {
        newExclusion.path = x.path.toLowerCase();
      }
      
      return newExclusion;
    });
  },
  push :function(item)
  {
    dataObjects.push(item);
  },
  
  removeDataObject : function(itemToRemove)
  {
    var tempItems = [];
    var i = 0;
    var output = [];
    for (i=0; i<this.dataObjects.length;i++)
    {
      var tempItem = this.dataObjects[i];
      if (tempItem != itemToRemove)
      {
        output.push(tempItem);
      }
    }
    
    this.dataObjects = output;
  },
  
  handleMaximizeButton : function(el)
  {
    com.b2a.log("Inserting buttons...");
    com.b2a.runShitCleanerFor([el]);
    chrome.extension.sendRequest({enable:true}, function(response) {});
  },
  
  refreshButtonsOnPage : function(collection, current)
  {
    if (collection.length == 1)
    {
      chrome.extension.sendRequest({enable:true}, function(response) {});
    }
    else if (collection.length == 2)
    {
      chrome.extension.sendRequest({enable:false}, function(response) {});
      com.b2a.ui.insertMaximizeButtons(collection, this.handleMaximizeButton);
    }
    else
    {
      chrome.extension.sendRequest({enable:false}, function(response) {});
      com.b2a.ui.insertMaximizeButtons([], this.handleMaximizeButton);
    }
  },
  
  handleElementInserted : function(event)
  {
    //com.b2a.log("found (on adding): " + event.target);
    if (com.b2a.isElement(event.target) && com.b2a.isShit(event.target))
    {
      com.b2a.log("Found some shit...");
      this.push(event.target);
      refreshButtonsOnPage(event.target);
      com.b2a.log("Showing Icon for new item! YAY! For VIMEO!");
    }
  },
  
  handleElementRemoved : function(event)
  {
    if (com.b2a.isElement(event.target) && com.b2a.isShit(event.target))
    {
      com.b2a.log("some shit goin away...");
      this.removeDataObject(event.target);
      refreshButtonsOnPage(event.target);
      com.b2a.log("Showing Icon for new item! YAY! For VIMEO!");
    }
  },
  
  runOnPage : function()
  {
    if (com.b2a.hasShit(this.dataObjects, false))
    {
      com.b2a.log("has one shit...");
      chrome.extension.sendRequest({enable:true}, function(response) {});
    }
    else if (this.dataObjects.length > 1)
    {
      com.b2a.log("has some shit...");
      com.b2a.ui.insertMaximizeButtons(this.dataObjects, this.handleMaximizeButton);
    }
    else
    {
      com.b2a.log("No Shit...");
      // No match was found.
    }
    
    var element = document.body;
    if (element.addEventListener)
    {
      com.b2a.log("adding listeners for new elements.");
      element.addEventListener('DOMNodeInserted', this.handleElementInserted, false);
      element.addEventListener('DOMNodeRemoved', this.handleElementRemoved, false);
    }
  },
  
  shouldRun : function(href)
  {
    var sr = true;
    var uri = new Uri(href);
    var currentHost = uri.host().toLowerCase();
    var currentPath = uri.path().toLowerCase();
    
    this.cleanExclusions().forEach(function(x) {
      
      if (x.host != null && x.path != null)
      {
        if (currentHost == x.host && com.b2a.string.startsWith(currentPath, x.path))
        {
          sr = false;
        }
      }
      else if (x.host != null)
      {
        if (currentHost == x.host)
        {
          sr = false;
        }
      }
      else if (x.path != null)
      {
        if (com.b2a.string.startsWith(currentPath, x.path))
        {
          sr = false;
        }
      }
    });
    
    return sr;
  }
}


if (com.b2a.contentPage.shouldRun(window.location.href))
{
  com.b2a.contentPage.runOnPage();
}


com.b2a["ui"] = {
  
  setToWindowSize : function(count)
  {
    if (count == 0 || count == null)
    {
      window.onresize = null;
      // document.body.style.height = "100%";
    }
    else
    {
      window.onresize = function(event) { com.b2a.ui.expandWindowToFit(count); };
      com.b2a.ui.expandWindowToFit(count);
    }
  },
  expandWindowToFit: function(count)
  {
    count = 1;
    document.body.style.height = "" + (com.b2a.ui.windowHeight() * count) + "px";
  },
  windowHeight: function()
  {
    var height = 0;
    var body = window.document.body;
    if (window.innerHeight)
    {
        height = window.innerHeight;
    }
    else if (body.parentElement.clientHeight)
    {
        height = body.parentElement.clientHeight;
    }
    else if (body && body.clientHeight)
    {
        height = body.clientHeight;
    }
    
    return height;
  },
  
  insertMaximizeButton : function(item, button, func)
  {
    imageUrl = chrome.extension.getURL('resources/expand-video-16.png'); 
    button.innerHTML = "<img src='" + imageUrl + "'/>";
    button.id = "MaximizeButton" + (Math.random() * 1000000);
    button.style.position = item.style.position;
    button.onclick = function()
    {
      func(item);
      return false;
    }
    
    var parent = item.parentNode;
    if (parent != null)
    {
      item.parentNode.appendChild(button);
    }
  },
  
  insertMaximizeButtons : function(items, func)
  {
    var i = 0;
    for (i=0; i< items.length; i++)
    {
      var item = items[i];
      var button = item.ownerDocument.createElement("button");
      this.insertMaximizeButton(item, button, func);
    }
  }
};

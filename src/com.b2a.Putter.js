
com.b2a["Putter"] = function()
{
  this.handleBodyStyling = function()
  {
    document.body.style.overflow = document.body.getAttribute(com.b2a.keys.overflow);
    document.body.removeAttribute(com.b2a.keys.overflow);
  };
  
  this.handleCompletedOfItem = function(el)
  {
  };
  
  this.handleChildTextStyling = function(el)
  {
  }
  
  this.handleElementStyling = function(el)
  {
    el.width = el.getAttribute(com.b2a.keys.width);
    el.height = el.getAttribute(com.b2a.keys.height);
    el.style.width = el.getAttribute(com.b2a.keys.styleWidth);
    el.style.height = el.getAttribute(com.b2a.keys.styleHeight);
    el.style.padding = el.getAttribute(com.b2a.keys.padding);
    el.style.margin = el.getAttribute(com.b2a.keys.margin);
    el.style.display = el.getAttribute(com.b2a.keys.visible);
    el.style.fontSize = el.getAttribute(com.b2a.keys.fontSize);
    el.style.position = el.getAttribute(com.b2a.keys.position);
    
    el.removeAttribute(com.b2a.keys.width);
    el.removeAttribute(com.b2a.keys.height);
    el.removeAttribute(com.b2a.keys.styleWidth);
    el.removeAttribute(com.b2a.keys.styleHeight);
    el.removeAttribute(com.b2a.keys.padding);
    el.removeAttribute(com.b2a.keys.margin);
    el.removeAttribute(com.b2a.keys.visible);
    el.removeAttribute(com.b2a.keys.fontSize);
    el.removeAttribute(com.b2a.keys.position);
  }
  
  this.handleChildElementStyling = function(item)
  {
    //&& item.getAttribute(com.b2a.key)=="true"
    if (item.hasAttribute(com.b2a.keys.visible))
    {
      item.style.display = item.getAttribute(com.b2a.keys.visible);
      item.removeAttribute(com.b2a.keys.visible);
    }
    else
    {
      //com.b2a.log("Item already turned" + item);
    }
  }
}

com.b2a.Putter.prototype = com.b2a.RecurserBase;
